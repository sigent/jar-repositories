#!/bin/bash

if [ -z ${CATALINA_HOME} ]; then echo "CATALINA_HOME is unset" && exit 1; fi

echo 'Downloading and replacing jars'
rm ${CATALINA_HOME}/webapps/spoon/WEB-INF/lib/sqlite-jdbc-*.jar
rm ${CATALINA_HOME}/webapps/spoon/WEB-INF/lib/h2-*.jar

wget -q https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/3.28.0/sqlite-jdbc-3.28.0.jar -O ${CATALINA_HOME}/webapps/spoon/WEB-INF/lib/sqlite-jdbc-3.28.0.jar || exit $?
wget -q https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar -O ${CATALINA_HOME}/webapps/spoon/WEB-INF/lib/h2-1.4.200.jar || exit $?
wget -q https://repo1.maven.org/maven2/org/osgeo/proj4j/0.1.0/proj4j-0.1.0.jar -O ${CATALINA_HOME}/webapps/spoon/WEB-INF/lib/proj4j-0.1.0.jar || exit $?
wget -q https://bitbucket.org/sigent/jar-repositories/raw/master/files/sigent-ftth-0.0.0.jar -O ${CATALINA_HOME}/webapps/spoon/WEB-INF/lib/sigent-ftth-0.0.0.jar || exit $?

echo 'End' || true